Random.self_init();;

type neurone = 
{
  inputs : float array;
  weights : float array;
}

let random_fill a =
  for i = 0 to Array.length a - 1 do
    a.(i) <- (Random.float 1.) -. 0.5
  done;
    a

let create n ={inputs = Array.make n 0.;weights = random_fill (Array.make n 0.)}

let sigma neurone =
  let s = ref 0. in
    for i = 0 to Array.length neurone.inputs - 1 do
      s := !s +. neurone.inputs.(i) *. neurone.weights.(i)
    done;
!s
                
let eval neurone f = f (sigma neurone)

let set_inputs neurone array_inputs =
        for i = 0 to Array.length neurone.inputs - 1 do
                neurone.inputs.(i) <- array_inputs.(i)
        done

let learn neurone f array_test array_res coef =
  let r = ref true in
  for i = 0 to Array.length array_test - 1 do
     set_inputs neurone array_test.(i);
     let output = eval neurone f in
     if (abs_float (output -. array_res.(i)) > 0.0001) then
     r := false;
     for j = 0 to Array.length neurone.weights - 1 do
       neurone.weights.(j) <- neurone.weights.(j) +. neurone.inputs.(j) *. coef *. (array_res.(i) -. output)
     done
  done;
     !r
        
let learn_all neurone f array_test array_res coef max print s=
   let i = ref 0 in
     while !i < max && not (learn neurone f array_test array_res coef) do
       print neurone f array_test array_res !i s;
       incr i
     done
                
let print_neurone neurone f s =
   s := !s ^ " Entrées du neurone : [|";
   Array.iter (fun x -> s := !s ^ string_of_float x ^ "000; ") neurone.inputs;
   let evalu neurone f = if (eval neurone f) = 0. then 1. else 0. in
   s := !s ^ "|] -> " ^ string_of_float (evalu neurone f) ^ "\n"
        
let print_learning neurone f array_test array_res n s=
   s := !s ^ "\n " ^ string_of_int (n+1) ^ " ieme phase d'apprentissage : ";
   s := !s ^ "Poids des liens arrivant au neurone: [|";
   Array.iter (fun x -> s := !s ^ string_of_float x ^ "; ") neurone.weights;
   s := !s ^ "|]\n";
   for i = 0 to Array.length array_test - 1 do
     set_inputs neurone array_test.(i);
     print_neurone neurone f s
   done                                                                         

let main () =
  let neur = create 3 and s = ref "" in
  let test_xor = [| [|1.;1.;1.|] ; [|1.;0.;1.|] ; [|1.;1.;0.|] ; [|1.;0.;0.|] |] in
  let res_xor = [| 1. ; 1. ; 1. ; 0. |] in
    let f x = if x < 1. then 0. else 1. in
      learn_all neur f test_xor res_xor 0.1 20 print_learning s;
      !s             
