(* Dimensions d'une image *)
let get_dims img =
  ((Sdlvideo.surface_info img).Sdlvideo.w, 
   (Sdlvideo.surface_info img).Sdlvideo.h)

(* Permet de loader en surface l'image *)
let load path =
  Sdlloader.load_image path

(* Permet de sauvegarder l'image dans le path *)
let save img path =
  Sdlvideo.save_BMP img path

(* Permet de vérifier si un pixel est bien dans les bornes *)
let is_in_bound img x y =
  let (w,h) = get_dims img in
    not ((x < 0) || (x > w - 1) || (y < 0) || (y > h - 1))

(* Permet de récupérer les pixels alentour de x y *)
let getallneighbors img x y =
  let result = ref [] in
    for i = (x - 1) to (x + 1) do
      for j = (y - 1) to (y + 1) do
         if is_in_bound img i j then
           result := (Sdlvideo.get_pixel_color img i j) :: !result
      done;
   done;
   !result
