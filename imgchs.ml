let bestimg src dst =
  let (w,h) = Imagehelp.get_dims src in
  let blacksrc = ref 0 and blackdst = ref 0 in
  for i = 0 to w - 1 do
    for j = 0 to h - 1 do
      let color1 = Sdlvideo.get_pixel_color src i j and
          color2 = Sdlvideo.get_pixel_color dst i j in
      if color1 = (0,0,0) then
        blacksrc := !blacksrc + 1;
      if color2 = (0,0,0) then
        blackdst := !blackdst + 1;
    done;
  done;
  if blacksrc > blackdst then
    src
  else
    dst
