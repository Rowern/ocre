type lchar =
    {
      mutable x :               int;
      mutable y :               int;
      mutable w :               int;
      mutable h :               int;
    }

type line =
    {
      mutable lx :               int;
      mutable ly :               int;
      mutable lh :  int;
      mutable lw  :       int;
      mutable lchar :   lchar list;
    }

type bloc =
    {
      mutable bx :              int;
      mutable by :              int;
      mutable bw :		int;
      mutable bh :             int;
      mutable lline :   line list;
    }

let init_bloc bloc =
  !bloc.bx <- 70000;
  !bloc.by <- 70000;
  !bloc.bw <- 0;
  !bloc.bh <- 0;
  !bloc.lline <- []

let init_line line =
  !line.lx <- 70000;
  !line.ly <- 70000;
  !line.lw <- 0;
  !line.lh <- 0;
  !line.lchar <- []

let parc_bloc m =
  let bllist = ref [] in
  let (larg,haut) = Imagehelp.get_dims m in
  let bloc = ref {bx = haut;by = larg; bw = 0; bh = 0; lline = []} in
  let is_line = ref false in
  let line_hgt = ref 0 in
  let space_nb = ref 0 in
    for y = 0 to haut - 1 do
      is_line := false;
      for x = 0 to larg - 1 do
        if Sdlvideo.get_pixel_color m x (y+1) = (0,0,0) then
          begin
            if !bloc.bx > x then !bloc.bx <- x else
              if !bloc.bw < x then !bloc.bw <- x;
            if !bloc.by > y then !bloc.by <- y else
              if !bloc.bh < y then !bloc.bh <- y;
            is_line := true;
          end;
      done;
      if not !is_line then
        space_nb := !space_nb + 1
      else
        begin
          line_hgt := !line_hgt + 1;
          if ((!line_hgt > 0 && !space_nb > !line_hgt) || y = haut - 1) then
            begin
              if y = haut - 1 then
                bllist := {bx = !bloc.bx; by = !bloc.by;bw = !bloc.bw;
			bh = !bloc.bh; 
                         lline = []} :: !bllist
              else
                bllist := {bx = !bloc.bx; by = !bloc.by; 
		bw = !bloc.bw;
                bh = !bloc.bh - (!space_nb +1);
		lline = []} :: !bllist;
              init_bloc bloc;
            end
          else
            if !space_nb > 0 then
              line_hgt := 0;
          space_nb := 0;
        end;
    done;
    bllist := {bx = !bloc.bx; by = !bloc.by;bw = !bloc.bw; bh = !bloc.bh;
             lline = []} :: !bllist;
bllist := List.rev !bllist;
!bllist

let is_white_line img b y =
   let rec iswl x =
      if x < b.bh then
         if Sdlvideo.get_pixel_color img x y = (0,0,0) then
              false
         else
              iswl (x+1)
      else
              true
        in iswl b.bx


let howmany_blacklines img b y =
  let rec hmbl n y =
    if not(is_white_line img b y) then
      hmbl (n+1) (y+1)
    else
       n
  in hmbl 0 y

let find_line img b =
  let l = ref [] in
  let rec fl y =
    if y < b.bh then
      begin
        if not(is_white_line img b y) then
	begin
	let nb_bl = howmany_blacklines img b y in
	  l := {lx = b.bx;ly = y -1;lw = b.bw
	  ;lh = y + nb_bl;lchar = []} :: !l;
	  fl(nb_bl + y);
	end
	else
	  fl(y+1);
     end
 in fl b.by;
l := List.rev !l;
!l

let parc_char m line =
  let is_column = ref false in
  let ch = ref { x = line.lw ;  y =  line.lh; w = 0;h=0} in
  for x = line.lx to line.lw - 1 do
    for y = line.ly to line.lh -1 do
     if Sdlvideo.get_pixel_color m x y = (0,0,0) then
     begin
     if !ch.x > x then !ch.x <- x else
         if !ch.w < x then !ch.w <- x;
     if !ch.y > y then !ch.y <- y else
         if !ch.h < y then !ch.h <- y;
     is_column := true;
     end;
    done;
    if !is_column = false && !ch.w <> 0 then
    begin
    line.lchar <- {x = !ch.x;y= !ch.y;w = !ch.w;h = !ch.h} :: line.lchar;
    ch := { x = line.lw ;  y =  line.lh; w = 0;h=0};
    end;
   is_column := false;
  done

let rec find_all_char img line =
  match !line with
  [] -> ()
  | e :: l -> begin
               parc_char img e;
               find_all_char img (ref l);
              end

let rec find_all_line img bllist =
  match !bllist with
  [] -> ()
 | e :: l -> begin
                e.lline <- find_line img e;
                find_all_char img (ref e.lline);
                find_all_line img (ref l)
                end


let print_char img line =
  let rec pc lch =
   match lch with
   [] -> ()
   |e :: l -> begin
                for j = e.x to e.w - 1 do
                Sdlvideo.put_pixel_color img j e.y (126,0,255);
                Sdlvideo.put_pixel_color img j e.h (126,0,255);
                done;
                for k = e.y to e.h - 1 do
                Sdlvideo.put_pixel_color img e.x k (126,0,255);
                Sdlvideo.put_pixel_color img e.w k (126,0,255);
                done;
                pc l
             end
  in pc line.lchar

let print_line img b =
  let rec pl line =
  match line with
  [] -> ()
  |e :: l -> begin
                for j = e.lx to e.lw - 1 do
                Sdlvideo.put_pixel_color img j e.ly (0,0,255);
                Sdlvideo.put_pixel_color img j e.lh (0,0,255);
                done;
                for k = e.ly to e.lh - 1 do
                Sdlvideo.put_pixel_color img e.lx k (0,0,255);
                Sdlvideo.put_pixel_color img e.lw k (0,0,255);
                done;
                print_char img e;
                pl l
             end
  in pl b.lline

let rec print_bloc img l =
      match l with
      [] -> ()
     |e :: l -> begin
                for j = e.bx to e.bw - 1 do
                Sdlvideo.put_pixel_color img j e.by (255,0,0);
                Sdlvideo.put_pixel_color img j e.bh (255,0,0);
                done;
                for k = e.by to e.bh - 1 do
                Sdlvideo.put_pixel_color img e.bx k (255,0,0);
                Sdlvideo.put_pixel_color img e.bw k (255,0,0);
                done;
                print_line img e;
                print_bloc img l;
                end

let rec clean_list l =
  match l with
  [] -> []
  | e :: l when (e.bw <= 0 || e.bh <= 0) -> l
  | e :: l -> e :: l

let rec print_list l =
  match l with
   [] -> ()
   |e ::l when e.lline <> [] -> Printf.printf "{ %i ; " e.bx;
                Printf.printf "%i ; " e.by;
                Printf.printf "%i ; " e.bw;
                Printf.printf "%i } " e.bh;
               print_list l
  | _ -> ()

let do_the_seg img = begin
  let l = ref (clean_list (parc_bloc img))  in
    print_list !l;
    find_all_line img l;
    print_bloc img !l;
end
