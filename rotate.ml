
let detect_ang src =
    let (width,height) = Imagehelp.get_dims src in
    let pi = 2. *. acos(0.) in
    let max_theta = ref 0. in
    let max_mat = ref 0 in
    let rho_max = int_of_float(sqrt(float_of_int(width*width+height*height))) in
    let mat = Array.make_matrix rho_max 360 0 in
    for j = 0 to height-1 do
        for i = 0 to width-1 do
            if (Sdlvideo.get_pixel_color src i j = (0,0,0)) then
                begin
                    let ang_var = ref(-.(pi/.2.)) in
                    while (!ang_var < (pi/.2.)) do
                        let rho = int_of_float(((float j) *. (cos !ang_var))+.
                        ((float i) *. (sin !ang_var))) in
                        if rho >= 0 then
                          begin
                            let theta = int_of_float(!ang_var*.100. +.
                            pi/.2.*.100.)in
                            mat.(rho).(theta) <-
                                mat.(rho).(theta)+1;
                                    if (!max_mat < mat.(rho).(theta)) then
                                        begin
                                         max_mat := mat.(rho).(theta);
                                         max_theta := !ang_var;
                                        end;
                          end;
                          ang_var := !ang_var +. 0.01;
                    done;
                end;
        done;
    done;
    (-.(!max_theta))

let rotation src dst angle =
    let (w, h) = Imagehelp.get_dims src in
    let imgcenterx = w / 2 in
    let imgcentery = h / 2 in
        for x = 0 to w - 1 do
          for y = 0 to h - 1 do
          begin
            let ca = cos angle in
            let sa = sin angle in
            let oldx = (((float_of_int (x - imgcenterx)) *. ca -.
                        float_of_int (y - imgcentery) *. sa)
                     +. float_of_int imgcenterx)
            and oldy = (((float_of_int (x - imgcenterx)) *. sa +.
                        float_of_int (y - imgcentery) *. ca)
                     +. float_of_int  imgcentery) in
            if (x < w) && (y < h) then
            begin
                if (oldx > 0.) && ((int_of_float oldx) < w)
                && (oldy > 0.) && ((int_of_float oldy) < h) then
                Sdlvideo.put_pixel_color dst x y
                (Sdlvideo.get_pixel_color src (int_of_float oldx)
                                            (int_of_float oldy))
                else
                Sdlvideo.put_pixel_color dst x y (255,255,255)
            end
          end;
     done;
  done

let redress src dst =
    let ang = detect_ang src in
    rotation src dst ang
