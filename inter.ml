let _ = GMain.init ()

let sdl_init () =
begin
    Sdl.init [`EVERYTHING];
    Sdlevent.enable_events
    Sdlevent.all_events_mask;
end

(* Fenêtre principale (non redimensionnable). *)
let window = GWindow.window
  ~width:800
  ~height:650
  ~resizable:false
  ~title:"OCRe Project" ()

(* Fonction pour créer un bouton avec label *)
let button_label label box callback =
    let button = GButton.button
    ~label:label
    ~packing:box#add () in
    ignore(button#connect#clicked ~callback:callback);
    button

(* Fonction pour créer un bouton avec stock *)
let button_stock stock box callback =
    let button = GButton.button
    ~stock:stock
    ~packing:box#add () in
    ignore(button#connect#clicked ~callback:callback);
    button

(* Conteneur principal, pour pouvoir insérer plusieurs widgets. En effet, les
 * fenêtres (GtkWindow) ne peuvent contenir qu'un seul enfant. *)
let vbox = GPack.vbox
  ~spacing:10
  ~border_width:10
  ~packing:window#add ()

(* Conteneur des boutons du haut *)
let bbox = GPack.button_box `HORIZONTAL
  ~layout:`SPREAD
    ~packing:(vbox#pack ~expand:false) ()

(* Conteneur des différents buffers *)
let hbox = GPack.hbox
  ~packing:vbox#add ()

(* Box pour l'image *)
let ibox = GPack.hbox
  ~spacing:5
  ~width:200
  ~border_width:5
  ~homogeneous:true
  ~packing:hbox#add ()

(*Ajoute l'affichage d'image *)
let image =
    let buf = GdkPixbuf.create
        ~width:500
        ~height:600 ()
    in
    GMisc.image
        ~pixbuf:buf
        ~packing:ibox#add ()

(* Box pour le texte*)
let tbox = GPack.hbox
  ~spacing:5
  ~border_width:5
  ~packing:hbox#add ()

let tag_tab = GText.tag_table ()
let buffer = GText.buffer
    ~tag_table:tag_tab
    ~text:"The text will be extracted here" ()

let text =
        let scroll =
                GBin.scrolled_window
                        ~hpolicy:`ALWAYS
                        ~vpolicy:`ALWAYS
                        ~shadow_type:`ETCHED_IN
                        ~packing:tbox#add ()
        in
    let basetext = "Résultat du réseau" in
    buffer#set_text(basetext);
        let textaux =
                GText.view
            ~buffer:buffer
                        ~packing:scroll#add ()
        in
        GtkSpell.attach ~lang:"fr_FR" textaux;
                textaux#misc#modify_font_by_name "Arial 10";
                textaux

(* Fenetre about (Précodez par GTK) *)
let about () =
    let dialog =
        GWindow.about_dialog
            ~authors:["TRESARRIEU Côme (tresar_c@epita.fr)";
            "BINEAU Bastien (bineau_b@epita.fr)";
            "THEVENOT Benoit (theven_b@epita.fr)"]
            ~copyright:"Copyright 2013-2014"
            ~license:"This is a public project!"
            ~version:"v1.0"
            ~website:"http://ocreproject.wordpress.com/"
            ~website_label:"OCRe dev blog"
            ~position:`CENTER_ON_PARENT
            ~parent:window
            ~destroy_with_parent:true ()
    in
         let fun_about () =
                ignore (dialog#run ());
                dialog#misc#hide ();
        in
                fun_about ()

(* Open a picture *)
let file_ok_sel filew () =
begin
    let img = Sdlloader.load_image filew#filename in
       Sdlvideo.save_BMP img "trans.bmp";
    let buffer = GdkPixbuf.from_file_at_size filew#filename
       ~width:500
       ~height:600
    in image#set_pixbuf buffer;
    print_endline "Image loading :  OK";
end

(* Save a picture *)
let save_image filew () =
    let img = Sdlloader.load_image "trans.bmp" in
    Sdlvideo.save_BMP img filew#filename

(* Image selcetion dialog *)
let selec f () =
    let selection = GWindow.file_selection
        ~title:"File selection"
        ~border_width:10
        ~filename:"image.bmp"
        ~destroy_with_parent:true ()
    in
    let fun_selec () =
        ignore(selection#ok_button#connect#clicked
            ~callback:(f selection));
        ignore(selection#cancel_button#connect#clicked
            ~callback:selection#destroy);
        ignore(selection#run ());
        selection#misc#hide ()
    in  fun_selec ()

let about_button =
    button_stock `ABOUT bbox about

let quit_button =
    button_stock `QUIT bbox GMain.quit

let openbutton =
    button_label "Open a file" bbox (selec file_ok_sel)

let savebutton =
    button_label "Save current" bbox (selec save_image)

(* Conteneur des boutons du bas *)
let bottombox = GPack.button_box `HORIZONTAL
    ~layout:`SPREAD
    ~packing:(vbox#pack ~expand:false) ()

let update_image () =
    let buffer = GdkPixbuf.from_file_at_size "trans.bmp"
        ~width:500
        ~height:600
    in image#set_pixbuf buffer;
    print_endline "Updating image successfully"

let test () =
    print_endline "PROUTDELABOUCHE"

let run_up f path =
begin
    f path;
    update_image ();
end

let grey () =
    run_up (Inter_fun.grey) ("trans.bmp")

let median () =
    run_up (Inter_fun.median) ("trans.bmp")

let convol () =
    run_up (Inter_fun.convol) ("trans.bmp")

let binarize () =
    run_up (Inter_fun.binarize) ("trans.bmp")

let rotation () =
    run_up (Inter_fun.rotation) ("trans.bmp")

let segment () =
    run_up (Inter_fun.segment) ("trans.bmp")

let network () =
    let str = Network.main () in
    text#buffer#set_text str;
    print_endline "Network :        OK"

let doall () =
    run_up (Inter_fun.run_all) ("trans.bmp");
    network ()

let doallbutton=
    button_label "Do all" bottombox doall

let greybutton =
    button_label "Greyscale" bottombox grey

let medianbutton =
    button_label "Median" bottombox median

let convolbutton =
    button_label "Gaussian" bottombox convol

let binbutton =
    button_label "Binarise" bottombox binarize

let rotate =
    button_label "Rotate" bottombox rotation

let segmentbutton =
    button_label "Segment" bottombox segment

let net =
    button_label "Network" bottombox network

let main () =
  sdl_init ();
  ignore(window#connect#destroy ~callback:GMain.quit);
  window#show ();
  GMain.main ()
