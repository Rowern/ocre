\newpage

Installation
============

OCRe est un projet réalisé exclusivement en OCaml. Il est donc necessaire
d'avoir OCaml déjà installer sur la machine.

De plus, comme décrit dans ce rapport, deux bibliothèques sont utilisées :
 - OCaml-SDL
 - LablGTK2
Il est donc obligatoire d'installer ces deux paquets ainsi que toutes leurs
dépendances.

De plus il est necessaire (s'il n'est pas déjà présent) de créer un 
dossier `result/`, qui accueillera les images "résultats" produit par
l'interface en ligne de commande (CLI).

Après avoir remplie chacune de ces spécifications il est possible de
compiler et d'exécuté le programme OCRe.

Compilation
-----------

Dans la tarball qui vous est fournit, un Makefile est présent, il offre
la possibilité d'une compilation rapide (un simple `make`) et permet le
nettoyage complet du dossier après compilation et utilisation de OCRe.

Toute fois il est possible de compiler notre projet à la main
(avec `ocamlopt`) mais il n'est pas garanti que la compilation se passe
sans accros.

Utilisation
-----------

Lors de l'éxecution du programme, deux choix s'offre à vous :
 - Execution en ligne de commande (CLI)
 - Execution en mode graphique

Un appel simple de OCRe (`./OCRe`) vous ouvrira l'interface graphique,
pour avoir accès à la CLI il faut appelé le programme de la manière
suivante : `./OCRe nomimage.(jpg,png,bmp)`
