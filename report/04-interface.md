\newpage

Interfaces
==========

Interface en ligne de commande (CLI)
------------------------------------

Pour la première soutenance, nous avions déjà créer une interface
en ligne de commande qui permettait à l'utilisateur d'entrée en
paramètre l'image qu'il souhaitait traité.

Notre interface se découpe de la manière suivante :
    ./OCRe image.(jpg,png,bmp)

Cela permet au programme d'effectué de manière autonome tous les
traitements les uns à la suite des autres. Evitant ainsi d'envoyer
à la fonction d'extraction une image qui n'aurait pas était
binarisé ou nettoyé.

De plus cette interface offre aussi la possibilité à l'utilisateur
de voir les différentes résultats de traitement que son image subi.

Cette interface offre aussi un autre avantage par rapport à
l'interface graphique : la possibilité de conserver (dans le
dossier result/) les résultats des différentes étapes de traitement
efféctué sur l'image passé en paramètre.

\newpage

Interface graphique
-------------------

L'interface graphique du projet OCRe, comme précisé dans le chapitre bibliothèque,
utilise LablGTK2.

Cette interface permet à l'utilisateur finale d'avoir un accès
simplifier et intuitif aux différentes fonctionnalitées de notre
projet.

###LablGTK

LablGTK est une interface OCaml pour GTK+. Elle fournit des fonctions
précodées qui permettent une création rapide d'une interface graphique
de qualité. Elle fonctionne sous forme de widgets qui communiquent entre
eux grâce à des "signaux". Ces signaux sont essentiel pour le bon
fonctionnement de l'interface. Par exemple, si l'utilisateur appuie
sur un bouton, un signal est envoyé qui permet l'execution d'une
fonction précise.

Malgré un manque d'exemple de compilation pratique, il nous fut simple
de bien comprendre et de réaliser une interface complète et fonctionnelle.

###Fonctionnement

La manière dont nous nous sommes servis de LablGTK pour réaliser
l'interface est simple à comprendre.
Tout d'abord nous disons à la bibliothèque de créer une fenètre.
Cette fenètre ne contient qu'une seule boîte qui contient toutes
les autres.
Après avoir donné à la fenètre principale ces différents attributs,
nous travaillons sur la boîte principale et non sur la fenètre.

Dans LablGTK, une boîte peut soit être verticale, soit horizontal.
Mais ces deux types de boîtes ont les mêmes propriétés. Ce sont en
réalité des containers qui permettent de contenir des widgets.

Chaque widget necessite d'être "attacher" à une boîte qui la
contiendra. Cette action s'effectue grâce à un attribut lors de la
création du widget : `~paking:(NomDeLaBoîteContenante)`.
LablGTK offre un volume relativement importants de type de widget
pré-défini. Par exemple, le widget d'affichage de l'image ou encore
le conteneur de boutons.

Pour expliquer le fonctionnement de GTK nous allons prendre une des
fonctions que nous avons codé et qui permet de mieux visualiser
comment créer un "objet" concret. Le code est le suivant :

	let button_label label box callback =
		let button = GButton.button
		~label:label
		~packing:box#add () in
		ignore(button#connect#clicked ~callback:callback);button

Comme vous pouvez le voir la fonction prend en parametre un label
(une chaine de caractère), une boîte (la boîte qui cotiendra) le bouton
qui sera créer par cette fonction et une fonction callback.
Cette dernière est une fonction qui sera appelé par le code si
l'utilisateur effectue un clique sur l'un des boutons.

###Composition de l'interface

OCRe possède une interface simple mais complète qui se décomposent
de la manière suivantes :

 - Les boutons outils

 - L'affichage de l'image

 - L'affichage du texte

 - Les boutons d'édition

####Boutons outils

Les boutons outils sont présent pour l'interraction avec
l'utilisateur. Ils offrent à ce dernier la possibilité de choisir
l'image à traiter, d'enregistrer le résultat ou encore d'en apprendre
plus sur le projet.
Pour la selection d'un fichier image, il fut necessaire de définir
une GWindow de type file_selection.
En effet, comme les conteneurs de boutons, cette fenètre est une
fonctionnalité déjà codé par LablGTK.
Elle permet, en plus d'éviter de recoder une fenètre, d'avoir la
possibilité d'obtenir un signal, et donc d'appeler une fonction,
lorsque l'utilisateur à selectionné un fichier. Cela permet de mettre
à jour l'affichage en conséquence.

![Boutons d'options](img/options.png)

#####Fenètre de selection

La fenètre de selection necessite deux fonctions :

 - Une fonction qui permet d'ouvrir la page de selection

 - Une fonction qui permet d'afficher l'image lorsqu'elle
 est selectionné

La première fonction a déjà été décrite un peu plus haut. Par
contre, la deuxième est essentielle.
Cette dernière offre la possibilité de mettre à jour l'affichage de
l'image. Elle est appelé après chaque modification sur l'image de
départ.
Après le chargement d'une image ou la modification de cette
dernière par l'une de nos fonctions, une image "trans.bmp" est
sauvegardé dans le dossier courant. Cela permet de conserver de
manière temporaire l'image qui est affiché par le pixbuf de la
boîte d'image.

#####Fenètre à propos

Nous avons aussi décidé d'afficher une petite fenètre "A propos"
qui permet à un utilisateur extérieur au projet, d'en apprendre un
peu plus sur le projet en lui même. Comme pour la selection, 
lablgtk offre une fenètre précodé, qui n'attend que certaines
informations en attribut pour sa création.

####Affichage image et texte

L'affichage de l'image est en réalité un buffer d'image inclus à
l'intérieur d'une boîte à image. Ce buffer est mis à jour à chaque
mise à jour de l'image. Ainsi, si l'utlisateur applique chaque
filtre à la main, il verra l'évolution et les mofications apportées
à l'image de départ. Par contre, si l'utilisateur décide de laisser
le programme effectuer tous les traitements les uns à la suite des
autres, il ne verra pas chaque étape mais simplement le résultat
final. \footnote{Cela permet au programme de s'exécuter plus rapidement}
Pour éviter d'altérer, de modifier ou de détruire l'image de
départ, notre programme ne travaille pas directement dessus mais
sur une copie locale qui sera utilisé tout au long de l'exécution des
différentes fonctions.
Un des très bon point de LablGTK est d'effectué de manière
automatique la redimension de l'image pour qu'elle puisse être
affiché entièrement dans le buffer.

Comme pour l'image, notre interface nous permet d'afficher un texte.
Cette zone de texte contient le résultat sous forme de `string` de notre
réseau de neurone.
De plus, cette "fenètre" en plus de contenir simplement du texte, offre la
possibilité d'une correction orthographique.
Cette correction orthographique est offert grâce au module GTKSpellcheck.
ce module prend en paramètre la langue du texte qui sera affiché (ici du
français) et le buffer sur lequel la correction doit s'effectuer.

####Boutons d'édition

Les boutons d'édition sont, comme le nom l'indique, les boutons qui
permettent à l'utilisateur de choisir les fonctions à appliquer sur
son image. Deux choix s'offrent à l'utilisateur : manuel ou
automatique. Le mode manuel necessite de cliquer sur chaque boutons
mais permet de voir l'évolution de l'image. Alors, que le mode
automatique permet l'affichage du résultat final.

![Boutons d'éditions de l'image](img/edition.jpg)

Au final, notre interface donne le rendu suivant :

![Image de l'interface](img/interface.png)
