\newpage

Binarisation d'une image
========================

Le réseau de neurones de n'importe quel OCR ne traite pas une image
comme elle est passé en paramètre. En effet, dans la partie précédente,
nous avons parlé des différents filtres appliqués à l'image pour la
rendre plus "lisible" par l'OCR. Dans cette partie nous parlerons de
la manipulation des couleurs d'une image, et plus particulièrement
de la transformation successives d'une image en niveau de gris puis
en binaire.
La binarisation, qui permet de remplacer chaque pixels par des pixels
blancs ou noirs, est utile car elle permet de faire ressortir l'élément
essentiel pour l'OCR, à savoir, le texte.

Niveaux de gris
---------------

![Image de base](img/basic.png)

Bien avant la binarisation il est necessaire de remplacer l'image de
base, celle qui sera traité par l'OCR, par la même image mais en
niveau de gris. Cette étape est simple mais essentiel pour avoir le
meilleur rendu possible pour la binarisation.

Pour cela il est necessaire de transformer chaque pixel de couleur
par son équivalent en gris.
Un pixel est défini par un triplet de : R,G,B (red,green,blue)
Nous appliquons donc le calcul suivant sur chaque pixel de l'image:
$$Y = 0.3 R + 0.59 G + 0.11 B$$

On obtient le résultat suivant :

![Image en niveau de gris](img/littlegrey.png)

\newpage

Seuil de binarisation
---------------------

Après avoir transformé l'image de base en sont équivalents de gris,
le passage en noir et blanc, l'étape de binarisation, peut commencer.
Le soucis est le suivant : Comment déterminer quelle valeur peut
être choisie comme seuil de l'image?

En effet, la binarisation necessite un seuil. Ce seuil est necessaire
dans le sens ou c'est lui qui détermine quels pixels seront passé en
noir et quels pixels seront mis en blanc.
La manière naïve d'implémenter le seuil est de le définir manuellement
entre 127-128 (la moitié de 255) et dire que tous pixel inférieur est
blanc ou sinon est noir.
Le soucis de cette implémentation est que le seuil est le même peu
importe l'image de base, ce qui pose problème car les images envoyés
au programme peuvent être très différente les unes des autres. La
couleur du papier, la couleur du texte ou encore la qualité du
document sont des paramètres qu'il est obligatoire de traiter pour
déterminer un seuil de bonne qualité.

Dans les deux parties qui suivent nous traiterons des deux
algorithmes qu'il est possible de mettre en place pour calculer un
seuil en fonction de l'image passé au programme.

\newpage

Calcul du seuil par la moyenne
------------------------------

La première méthode qui fut implémentée dans notre projet fut celle
du seuil par la moyenne.
Cette méthode nous permettais de récupérer la moyenne de toutes les
couleurs contenue dans une image :

$$moyenne = \frac{1}{n}{\sum_{i=1}^{n} e_i}$$

Cette moyenne était ensuite défini comme le seuil pour la binarisation.

Bien que très intuitive et simple à mettre en place cette méthode
n'était pas suffisamment précise pour supprimer le bruit et
conserver les propriétés importantes d'un document texte.
C'est pourquoi, dans un second temps, nous nous sommes tourné vers
un deuxième algorithme : la méthode d'Otsu.

Méthode d'Otsu
--------------

La méthode d'Otsu est utilisé pour effectuer un seuillage 
automatique d'une image à partir de la forme de l'histogramme de
l'image. L'algorithme de cette méthode est le suivant:

1. Création de l'histogramme des niveaux de gris de l'image
2. Normalisation de l'histogramme obtenue
3. Calcul de la valeur moyenne de l'histogramme
4. Calcul de la variance de l'histogramme
5. Calcul final du seuil

L'histogramme d'une image est un concept simple qui est indispensable
à la méthode d'Ostu. L'histogramme est, en fait, un tableau de 256
cases. Ce qui permet de représenter n'importe quel valeur obtenu par
le passage en niveau de gris.
Pour remplir l'histogramme il est necessaire de parcourir toute
l'image et d'incrémenter de 1 le nombre contenue dans la case
correspondante du tableau.
Exemple : si le pixel est défini par (9,9,9) alors la case numéro
9 de l'histogramme sera incrémentée de 1.
On obtient ainsi un tableau contenant les valeurs de chaque niveau
de gris.

![Histogramme](img/histograms.png)

La deuxième étape est celle de la normalisation de l'histogramme.
Cette étape nous permet de transformer les valeurs contenue dans
l'histogramme par des valeurs comprisent entre 0 et 1, cela permet
d'interpréter chaque case comme la probabilité d'occurence de la
couleur dans l'image, facilitant ainsi le traitement de l'histogramme
Cette valeur est calculée de la manière suivante :

$$h[i] = \frac{h[i]}{H \times L}$$
avec $h[i]$ la ieme case de l'histogramme, $H$ est la hauteur et
$L$ est la largeur de l'image

Après cette étape il est possible de calculer la moyenne et la
variance de l'histogramme obtenue.
Le calcul de la moyenne est le suivant :

$$\mu = \sum_{k = 1}^{n} k \times h[k]$$
avec $h[k]$ la kieme case de l'histogramme et $n$ le nombre total
d'éléments dans l'histogramme (256)

Le calcul de la variance est le suivant :
$$\omega = \sum_{k = 1}^{n} h[k]$$
avec $h[k]$ la kieme case de l'histogramme et $n$ le nombre d'éléments
contenue dans h.

Après tous ces calculs il ne reste plus qu'à appliquer la
formule d'Otsu :

$$s[i] = \omega[i] \times (1 - \omega[i]) \times (\mu[255] \times \omega[i] - \mu[i])^2$$

avec $i$ $\in$ $[1,255]$, $\omega[i]$ la variance et $\mu[i] moyenne
d'un histogramme composé de $i$ éléments.
Le seuil est ensuite donné par :
$$threshold = max(s[i])$$

Le seuil est ainsi trouvé. Il ne reste plus qu'à binariser l'image
grâce à ce dernier.

\newpage

Binarisation
------------

Grâce au seuil passé en paramètre à la fonction de binarisation
permet de transformer une image de niveau de gris en une image dites
binaire car composé uniquement de pixel blanc ou de pixel noir.
Pour cela, la fonction vérifie si chaque pixel est composée de valeurs
supérieurs au seuil et si tel est le cas, transforme ce pixel en
pixel noir sinon en pixel blanc.
L'image est donc finalement uniquement composée de pixel blanc ou noir.

On obtient le résultat suivant :

![Image binarisé](img/medbin.png)

\clearpage
