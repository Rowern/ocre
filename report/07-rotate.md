\newpage

La rotation
===========

Dans le pré-traitement de l'image il nous faut effectuer une rotation dans le
cas ou une image ne serait pas droite. Pour cela il nous faut donc détecter
l'angle pour lequel l'image serait remise droite et ensuite effectuer la
rotation de celle-ci avec l'angle trouvé précédemment.

Détection de l'angle
--------------------

Pour cette deuxième soutenance nous avons donc réussi à mettre en place la
détection à l'aide de la transformée de Hough, qui nous permettra de
reconnaître les lignes d'une image. Cette méthode consiste à supposer plusieurs
droites passant par un même point et de calculer leur angle et leur norme qui
nous servirons à détecter les lignes du texte et de déterminer le degré de
décalage de ces lignes.

Pour cela nous allons donc effectuer la détection aprés la binarisation car
nous allons parcourir chaque pixel de notre image et seulement lorsque nous
rencontrerons un pixel noir nous effectuerons l'algorithme de Hough.
Donc une fois que l'on arrive sur un pixel noir, pour plusieurs angles
variant de 90° à -90°, nous allons calculer ça norme rho grâce à la formule
suivante :

$$rho = y \times \cos \alpha + x \times \sin \alpha$$

Ensuite si la norme est positive, nous devrons incrémenté notre matrice que nous
aurions créé auparavant. La matrice réprésente la norme en abscisse et l'angle
en coordonnée, ainsi, pour
tous les pixels noirs que nous parcourons, nous ajouterons 1 à chaque
coordonnée (norme, angle) que nous calculon.
Une fois cela fait nous chercherons la valeur maximal de notre matrice. Nous
obtiendrons donc l'angle qui correspond à celui du  décalage des
lignes de texte.

Une fois cette angle obtenue nous pourrons l'associer avec la rotation pour
obtenir le bon redressement de l'image.

![Diagramme de la transformation de Hough](img/hough.png)

\newpage

Méthode de rotation
-------------------

Pour la rotation je suis tout d'abord partit du principe de parcourir mon image en largeur et en hauteur afin de prendre chaque pixel et de calculer ces nouvelles coordonnées grâce aux opérations suivantes :

$$nouvelle abscisse = x \times \cos \alpha - y \times \sin \alpha$$
$$nouvelle ordonnée = x \times \sin \alpha - y \times \cos \alpha$$

Cependant un problème est vite apparue car l'image après rotation n'était pas
affiché entièrement. Je me suis donc aperçu que le problème venait du centre de rotation qui était donc aux coordonnées (0,0) alors qu'il devrait être au centre de l'image afin de garder toute l'image après rotation. Pour cela j'ai procédé comme précédemment mais avec un calcul différent qui est celui-ci :

$$nouvelle abscisse = \cos \alpha \times (x - largeur / 2) - \sin \alpha \times (y - hauteur / 2) + largeur / 2$$
$$nouvelle ordonnée = \cos \alpha \times (y - hauteur / 2) + \sin \alpha \times (x - largeur / 2) + hauteur / 2$$

Malgré ces modifications j'ai rencontré un nouveau problème : l'image
effectuait la rotation seulement à moitié et l'autre moitié formé un cercle
avec des répétions de même pixels. J'ai vite compris que cela était du au faite
que lors de la rotation les nouvelles coordonnées des pixels écrasaient
d'autres pixel. La rotation s'effectuait sur la même image donc sur plusieurs même pixel que l'on voyait donc apparaitre, ce qui donnait un résultat étrange. Il m'as donc juste fallu envoyer les pixels à leur nouvelle place dans une nouvelle surface et voila la rotation était faite parfaitement.

![Avant et après appliquation de la rotation](img/Rotation1.png)
