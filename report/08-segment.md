
\newpage 

Detection des zones de textes
============================= 

On suppose que l'image passée en paramètre est "parfaite" et a subi les traitements précedents.
Dans cette partie, il y a trois détections à faire. La première est la détection 
des blocs de la pages, c'est-à-dire les différents paragraphes du texte, le titre, la signature...
La seconde est la détection des lignes, qui revient à repérer les lignes dans les 
paragraphes. Enfin, la dernière est la détection des caractères, qui est nécéssaire 
pour la suite du projet, puisqu'il faut avoir découpé les caractères pour pouvoir 
les reconnaîtres. C'est alors que je me suis posé une question sur ce qu'est un caractère,une ligne et un paragraphe dans une image.

Ils correspondent tous à une zone de l'image. Je me suis donc inspiré du type Rectangle de XNA que nous utilisions régulièrement pour le projet de sup.
Ainsi chacune de ces zones de texte sera de la forme: (x,y,w,h) . Cependant si on utilise la même forme pour tout ces types comment les différencier par la suite?
Il suffit de penser maintenant qu'un bloc est composé d'une ou plusieurs lignes et qu'une ligne est elle même composée de caractères. La première chose à faire avant 
de découper l'image est donc de définir trois enregistrements pour ces types:

    type lchar =
    {
    mutable x : int;
    mutable y : int;
    mutable w : int;
    mutable h : int;
    }
    type line =
    {
    mutable lx : int;
    mutable xy : int;
    mutable lw : int;
    mutable lh : int;
    mutable lchar : lchar list;
    }
    type bloc =
    {
    mutable bx : int;
    mutable by : int;
    mutable bw : int;
    mutable bh : int;
    mutable lline : line list;
    }


Détection des blocs
-------------------

Avant tout, il nous faut détecter les différents blocs dans le texte (para-graphes, titre...)

Pour cela on procède en deux étapes:

La première consiste à recupérer les blocs en parcourant horizontalement l'image (De gauche à droite et de haut en bas), 
on initialise un bloc qui va nous permettre de venir encadrer les parties horizontale du texte puis dès que l'on trouve un pixel noir on
 met à jour les coordonnées de ce bloc. Ensuite pour toute les lignes contenant au moins un pixel noir, on va incrémenter un compteur qui
 va correspondre à la hauteur d'une ligne de ce bloc. De même dès que l'on trouve une ligne complètement blanche, on incrémente un compteur de lignes blanches.

Si l'on est sur une ligne blanche, c'est que l'on est entre deux lignes ou après un paragraphe. Si l'on est après un paragraphe alors on met le bloc actuel dans
 une liste de blocs.Pour savoir si l'on est entre deux lignes ou après un paragraphe on compare le nombre de lignes blanches consécutives à la hauteur d'une ligne du bloc
 si celui est supérieur à la hauteur, alors on est en fin de paragraphe. On répète ainsi ce procédé jusqu'a obtenir une liste contenant tous les blocs de la page.

 Cependant, cette liste est montée à l'envers (les blocs du bas en premier), on retourne donc la liste pour les obtenir dans l'ordre.
La deuxième etape consiste à parcourir verticalement les blocs de la liste pour voir si il n'y a pas deux paragraphes parrallèles en colonnes. Pour cela on parcourt verticalement chaque bloc de la liste 
et lorsque l'on trouve une colonne complètement blanche dans un bloc alors on découpe le bloc de façon à récupérer les blocs séparéments et les inserer à la suite dans notre liste de blocs.



Détection des lignes
--------------------

Pour chaque bloc de la liste de bloc récupérée précédemment on applique le traitement suivant:
On parcourt complètement la liste de lignes et on récupère toute les coordonées du bloc. On va ensuite parcourir verticalement tous les pixels d'un bloc (De gauche à droite et de haut en bas).
Si l'on tombe sur une ligne y contenant un pixel noir alors on compte le nombre de lignes consécutives contenant du noir à partir de la ligne où l'on trouve le premier pixel noir.
On ajoute alors une ligne de coordonnée (x du bloc, y , w du bloc , h (hauteur d'une ligne)) dans la liste de ligne du bloc. On répète ceci jusqu'a récuperer toutes les lignes d'un bloc.
On récupère ainsi toute les lignes d'un bloc. Et donc toutes les lignes du texte .

![Image après detection des lignes et des blocs](img/segment.png)

\newpage

Détection des caractères
------------------------

La détection des caractères est désormais fonctionelle pour cette soutenance.
Pour chaque ligne de la liste de lignes d'un bloc récupérée précédemment on applique le traitement suivant:
On parcourt la ligne verticalement pour que le découpage de caractères soit fonctionnel.
On initialise un caractère qui va nous permettre de venir encadrer les caracteres du texte puis dès que l'on trouve un pixel noir on met à jour les coordonnées de ce caractère. Enfin lorsque l'on tombe sur une colonne blanche qui succède à une ou plusieurs colonnes contenant au moins un pixel noir. Alors on insère le caractère dans la ligne et on réinitialise le caractère de construction
et on continue le parcours. 

 Cependant, cette liste est montée à l'envers (les blocs du bas en premier), on retourne donc la liste pour les obtenir dans l'ordre.

Tracé des différents découpages
-------------------------------

Lors du découpage, on décide de tracer les contours des éléments découpés afin d'avoir une meilleure vision du travail effectué par la machine. Nous avons donc décidé d'utiliser un code couleur simple afin de différencier les blocs, les lignes et les caractères. Le rouge correspond aux contours des blocs, le bleu aux contours des lignes et enfin le mauve pour le découpage final des caractères.

Normalisation des caractères pour le passage aux neurones
---------------------------------------------------------

Pour que la creation d'un reseau de neurones soit possible on décide de normaliser les caractères. C'est à dire que chaque caractère doit faire la meme taille. On choisit donc de les transformer tous de façon à ce que les neurons d'entree puissent les lire. La taille choisit est de 16px * px.

![Image du découpage complet](img/segmentall.png)

\newpage
