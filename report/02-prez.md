\newpage

Présentation des membres du groupe
==================================

Bastien Bineau (Segmentation/Reseau de neurones)
--------------

Salut ! Moi c'est Bastien j'ai 19 ans et je suis désormais en spé à 
EPITA.
J'entame donc ma deuxieme année ainsi que mon second projet avec de 
nouveaux acolytes.
Ce n'est pas pour autant que je suis un "pro" de la programation mais je pense néanmoins avoir appris un grand nombre de connaissances depuis l'année dernière (ce début d'année y compris avec l'arrivée de système UNIX) que je pourrais mettre en place durant ce projet.
Je trouve le projet OCR réellement intéressant, le fait d'avoir un cahier des charges à respecter (qui n'est pas de nous) renforce l'idée que j'ai de la réalisation de projets en entreprise.
J'ai d'ailleurs été nommé chef de projet par le reste de notre petite équipe (Ai-je eu le choix? Je ne sais pas :) ).
C'est donc ma seconde experience en tant que chef de projet à l'EPITA, une experience qui ,je l'espère , m'aidera fortement dans ma vie professionelle à venir.


Benoit Thévenot (Rotation / Site web)
---------------

Originaire d'une petite ville nommée Nevers, je suis donc partit à
l'aventure dans la région parisienne pour étudier à EPITA, ce qui
m'a emmené à faire ce projet.
La création d'un OCR sera pour moi l'opportunité d'apprendre de
nouvelles choses et me permettra de mettre en application ce dont
on apprend en cours ou de rechercher par soi-même ce qui peut nous
êtres utiles à un projet.
Je m'occuperais pour cette première soutenance de la rotation dans
le pré-traitement de l'image.


Côme Tresarrieu (Filtres / Interface)
---------------

Je suis élève à EPITA en Info-SPE et je fais partie de ce groupe
de projet dont le but est de réaliser un OCR.
Je suis heureux de réaliser ce projet, l'année dernière j'étais en
charge de la création de l'Intelligence Artificiel dans le cadre du
projet de SUP, et j'ai vite prix gout au développement des IAs.
Ce projet est donc parfaitement adapté pour moi car la partie de
l'intégration d'un réseau de neurones ne peut que m'apporter plus de
connaissances dans le domaine qui m'interresse.
