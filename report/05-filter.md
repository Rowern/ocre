\newpage

Les filtres
===========

Les filtres sont essentiels pour un système de reconnaissance de
caractère. En effet pour permettre une bonne extraction des données
par un réseau de neurones, il est necessaire de nettoyer et de
rendre l'image la plus lisible possible. Même si, de nos jours, 
les scanneurs sont de plus en plus puissant pour produire des 
images de document de très bonne qualités, il n'est pas impossible 
que le document en lui même ne soit pas directement utilisable pour
l'extraction de donnée.

Pour cette première soutenance nous avons réussi à intégrer tous les
différents filtres demandé par le cahier des charges. Nous
détaillerons ici leur mise en place.

Le filtre médian
----------------

Le premier filtre que nous avons intégré dans notre projet fut le
filtre médian. En effet, ce dernier offre un véritable nettoyage de
l'image. Il est même souvent cité dans les articles portant sur les
OCR. Le filtre médian possède un réel intérèt car il n'altère pas
trop les informations contenue par un document texte.
Cependant, si ce filtre est appliqué sur une image de taille
moyenne ou petite, alors dans ces cas là la perte d'information est
trop importante.

Le principe de ce filtre est parcourir toute l'image, pixel par
pixel, en remplaçant chaque entrée par la médiane des entrées
voisines.

Pour effectuer cela nous prenons une "fenètre" de $3 \times 3$
qui passe sur chaque pixel d'une image et permet de récupérer les
pixels voisins du centre de la fenètre.
Par exemple si nous somme sur un pixel situé en place (i , j) d'une
image, la fenètre nous permettra de récupérer la liste l suivante:

$$l = \{(i - 1,j + 1);(i,j + 1);(i - 1,j + 1);(i - 1,j);(i,j);(i + 1,j);(i - 1,j - 1);(ij - 1);(i + 1,j - 1)\}$$

Cette liste est ensuite trièe par rapport à la valeur du niveau de gris de
chacun des pixels. On cherche alors la valeur médiane de cette liste trièe.
La médiane est contenue dans la case situé en liste.taille / 2.
On remplace ensuite le pixel courant par la veleur de la médiane.

\newpage

Le filtre de convolution
------------------------

Le filtre de convolution permet d'appliquer à une image n'importe quel
matrice. Cela permet par exemple de flouter, donc d'enlever du bruit, d'une image.

Nous avons décider d'ajouter ce filtre dans le but de pouvoir faire un
choix entre le médian et un autre filtre, plus fin. Cela nous évitera de
toujours appliqué le filtre médian qui n'est pas toujours adapté à l'image
à laquelle il est appliqué.

Pour appliquer à une image le filtre de convolution il est necessaire de
considérer l'image comme une matrice. En effet, ce filtre prend en paramètre
l'image et un tableau à deux dimensions qui contient des coefficients. Comme SDL nous permet d'accéder au pixel comme dans un tableau à
deux dimensions, nous avons pensé qu'il n'était pas necessaire de
transformer l'image en matrice.

Faire la convolution de l'image revient à faire comme pour le filtre
médian, il suffit de récupérer les pixels adjacents de l'actuel et
de leur appliquer le coefficient présent dans le tableau à deux
dimensions et d'ensuite mettre la moyenne du résultat dans le pixel 
sur lequel la "fenetre" est placé

Le tableau à deux dimensions que nous avons décider d'appliquer à 
l'image est de type gaussien. Il donne de bon résultat sur les images
de petite taille mais donne des résultats comparables au filtre médian
sur les grandes images.
Le tableau utilisé est de la forme :
$$_M{Gaussian}^{3 \times 3} =
\begin{pmatrix}
1 & 2 & 1 \\
2 & 4 & 2 \\
1 & 2 & 1
\end{pmatrix}$$
