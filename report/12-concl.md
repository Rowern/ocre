\newpage

Conclusion
==========

Cette deuxième soutenance annonce la fin du semestre et donc de ce
projet. OCRe fut une aventure enrichissante pour chacun d'entre
nous. Offrant par exemple la possibilité à certain de mieux
comprendre la syntaxe du OCaml.

Ce projet, fut aussi, pour nous, un réel défi. En effet, nous ne
sommes que trois, un choix qui ne parut pas trop handicapant au
début de l'année mais qui se révéla bien génant pour la répartition
des tâches au sein du groupe pour cette dernière soutenance.

La plus grande difficulté, à laquelle nous avons fait face, est la
compréhension et la mise en place d'un réseau de neurone.
Pour la première soutenance il nous étais demandé de présenter un
proof of concept d'un réseau de neurone. Malheureusement il ne nous
fut pas possible d'en réaliser un à temps.
Pour cette dernière soutenance, après de multiples essais, demande
d'aide à des camarades, nous avons pu créer un réseau de neurone
relativement simple qui permet de reconnaître un XOR.

Bien que nous n'ayons pas réussi à créer un réseau de neurones qui
apprend et reconnait des cractères nous sommes heureux de dire que
toutes les étapes qui précède un traitement par un réseau de
neurone sont accomplis. Les filtre marchent parfaitement bien,
la binarisation est complètes et s'adapte par rapport à l'image,
la rotation donne des résultats très satisfaisant gâce à
l'algorithme de Hough et enfin, l'extraction des caractères donne
de très bon résultat sur les images ayant une grande taille.
Nous sommes donc dans l'ensemble heureux d'avoir réalisé ce projet
et de l'avoir amené jusqu'à ce point.
