\newpage

Choix des bibliothèques
=======================

Contrairement au C#, où les images peuvent être géré de base par les
bibliothèque fournies avec Visual Studio, Ocaml ne fournit aucun 
pré-intégré pour la gestion des images.

Il était donc necessaire de faire un choix quant à la bibliothèque que
notre projet exploiterait pour ouvrir, modifier ou encore enregistrer
des images.

Dans un premier temps, nous nous étions tourné vers CamlImage.
Après plusieurs essais d'installation, tant bien manuelle que par
gestionnaire de paquet, nous avons décidé de passer sur la
bibliothèque la plus utilisé pour la gestion des images : OCaml-SDL

L'installation et l'intégration de SDL dans ocaml se déroula 
beaucoup plus simplement et rapidement que pour notre premier choix.
De plus, comme le cahier des charges le précise, une interface graphique
est demandé pour avoir le meilleur rendu possible.
C'est pour cela que nous avons décidé d'installer et d'utilisé une autre
bibliothèque : GTK2.

Dans les deux parties qui suivent nous vous décrirons la manière dont
nous utilisons ces librairies pour avoir un programme fonctionnel.

Description de SDL
------------------

OCamlSDL est une bibliothèque qui offre une interface entre le langage
de programmation ocaml et la bibliothèque C de SDL.

SDL signifie : Simple DirectMedia Layer. C'est une API(Interface de programmation)
qui offre un acces bas niveau à de l'audio, de la video ou des entrées
clavié/souris.
Cette bibliothèque est considéré comme un standard pour le développement
d'application, necessitant du multimédia, sur les plateformes
GNU/Linux.

En d'autre terme, OCamlSDL nous offre des outils puissants de manipulations
d'images ainsi qu'une syntax très simple et lisible.

Dans notre projet, SDL joue un rôle très important. En effet, cette
dernière est au coeur du pré-traitement de l'image.
Pour éviter un surplus de calcul, nous avons pris le partie de ne
pas transfomer notre image en matrice avant qu'elle ne soit complètement
prête pour l'envoie au réseau de neurone. Nous avons donc décider dedirectement
traité l'image grâce à SDL.

\newpage

Description de GTK2
-------------------

OCRe necessite lablgtk2 pour afficher son interface.
Cette interface, demandé dans le cahier des charges, est un moyen pour
l'utilisateur finale d'avoir accès de manière rapide et clair au
différentes fonctionnalité de notre projet.

LablGTK est l'implémentation Ocaml de GTK+. GTK+ (The GIMP Toolkit) est un
ensemble de bibliothèques logicielles, c'est-à-dire un ensemble de fonctions
permettant de réaliser des interfaces graphiques. Cette bibliothèque a été
développée originellement pour les besoins du logiciel de traitement d'images
GIMP.

Comme SDL cette bibliothèque (originalement codée en C) offre une interface
en le langage OCaml et la bibliothèque GTK.

Le principale avantage de cette bibliothèque par rapport à toute les autres
est le fait qu'elle soit rapide à prendre en main.
Le défaut majeur de LablGTK est sa documentation relativement dure à
comprendre.
Effectivement, étant une interface OCaml/C, la documentation nous renvoie sur
la version C, qui possède une syntaxe complètement éloignée.
