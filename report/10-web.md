\newpage

Site web
========

Comme demandé par le cahier des charges, nous avons mis en place un
site web qui permettra de tenir les gens extérieurs au groupe de notre
avancement dans la projet.

Pour nous faciliter la vie, et ainsi nous focaliser sur le développement
de l'application, nous avons fait le choix de créer notre site sous
wordpress.

Wordpress est un CMS (Système de gestion de contenu) qui permet de
créer un blog avec un système de ticket performant et facile à
mettre en place.

![Notre site web](img/web.jpg)

