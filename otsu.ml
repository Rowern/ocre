(* Permet de récupérer l'histogramme des pixels noir *)
let histogram img =
  let (w,h) = Imagehelp.get_dims img in
  let tab = Array.make 256 0 in
  for i = 0 to w - 1 do
    for j = 0 to h - 1 do
      let (r,_,_) = Sdlvideo.get_pixel_color img i j in
        tab.(r) <- tab.(r) + 1
    done;
  done;
  tab

(* Permet de normaliser les vals de histo (évite d'avoir nawak) *)
let histo_normalization histo img =
  let result = Array.make 256 0. in
  let (w,h) = Imagehelp.get_dims img in
  let size = w * h in
    for i = 0 to 255 do
      result.(i) <- (float_of_int histo.(i) /.
		     float_of_int size)
    done;
    result

(* Renvoie la valeur moyenne de classe de l'histo *)
let get_mu histo =
  let result = Array.make 256 0. in
    for i = 1 to 255 do
      result.(i) <- result.(i - 1) +.
		    float_of_int i *. histo.(i);
    done;
    result

(* Renvoie la variance de classe de histo *)
let get_omega histo =
  let result = Array.make 256 0. in
    result.(0) <- histo.(0);
      for i = 1 to 255 do
	result.(i) <- result.(i - 1) +. histo.(i);
      done;
      result

(* Calcul de l'exponentiel (a^b) *)
let expo a b =
  let rec expo_rec b i = match b with
     | 0.0 -> i
     | _ -> expo_rec (b -. 1.0) (a *. i)
  in
  expo_rec b 1.0

(* Calcul pour le sigma *)
let sigma_value mu omega i =
  (expo (mu.(255) *. omega.(i) -. mu.(i)) 2.0)
  /. (omega.(i) *. (1.0 -. omega.(i)))

(* Calcul le seuil par rapport au valeur des params *)
let threshold_value sigma omega mu =
  let result = ref 0 and
      max_s = ref 0.0 in
    for i = 0 to 254 do
      if ((omega.(i) <> 0.0) && (omega.(i) <> 1.0)) then
	sigma.(i) <- sigma_value mu omega i
      else sigma.(i) <- 0.0;
      if sigma.(i) > !max_s then
	begin
	  max_s := sigma.(i);
	  result := i
	end
    done;
    !result

(* Le "main" otsu *)
let threshold_img img =
  let histo = histogram img in
  let normalizedhisto = histo_normalization histo img in
  let mu = get_mu normalizedhisto in
  let omega = get_omega normalizedhisto in
  let sigma = Array.make 256 0.0 in
  threshold_value sigma omega mu
