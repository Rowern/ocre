(* init de SDL *)
let sdl_init () =
  begin
    Sdl.init [`EVERYTHING];
    Sdlevent.enable_events Sdlevent.all_events_mask;
  end

(* attendre une touche ... *)
let rec wait_key () =
  let e = Sdlevent.wait_event () in
    match e with
    Sdlevent.KEYDOWN _ -> ()
      | _ -> wait_key ()

(*
  show img dst
  affiche la surface img sur la surface de destination dst (normalement l'écran)
*)
let show img dst =
  let d = Sdlvideo.display_format img in
    Sdlvideo.blit_surface d dst ();
    Sdlvideo.flip dst

(* main *)
let maincommande () =
  begin
    (* Nous voulons 1 argument *)
    if Array.length (Sys.argv) > 3 then
      failwith "Too much parameter. Refer to README!";
    (* Initialisation de SDL *)
    sdl_init ();
    (* Chargement d'une image *)
    let img = Sdlloader.load_image Sys.argv.(1) in
    (* On récupère les dimensions *)
    let (w,h) = Imagehelp.get_dims img in
    (* On crée la surface d'affichage en doublebuffering *)
    let display = Sdlvideo.set_video_mode w h [`DOUBLEBUF] in
      (* on creer la nouvelle surface *)
      let surf1 = Sdlvideo.create_RGB_surface_format img [] w h and
          surf2 = Sdlvideo.create_RGB_surface_format img [] w h and
          surf3 = Sdlvideo.create_RGB_surface_format img [] w h in
      (* on affiche l'image *)
      show img display;
      wait_key ();
      (* Niveau de gris *)
      Filters.image2grey img surf1;
      Filters.image2grey img surf2;
      Imagehelp.save surf1 "result/grey.bmp";
      show surf1 display;
      wait_key ();
      (* Applique le filtre median *)
      Filters.median_filter display surf1;
      Imagehelp.save surf1 "result/median.bmp";
      show surf1 display;
      wait_key ();
      (* Filtre de convolution avec le gaussien *)
      let gauss = Filters.gaussian in
      Filters.convolution_filter surf2 surf2 gauss;
      Imagehelp.save surf2 "result/gaussian.bmp";
      show surf2 display;
      wait_key ();
      (* Binarization de l'image en niveau de gris *)
      Filters.binarize surf1;
      Imagehelp.save surf1 "result/medbin.bmp";
      Filters.binarize surf2;
      Imagehelp.save surf2 "result/gaussbin.bmp";
      show surf2 display;
      wait_key ();
      Rotate.redress surf2 surf3;
      show surf3 display;
      Imagehelp.save surf3 "result/rotate.bmp";
      wait_key ();
      Segment.do_the_seg surf3;
      Imagehelp.save surf3 "result/segment.bmp";
      show surf3 display;
      wait_key ();
      print_endline (Network.main ());
      (* on quitte *)
      exit 0
  end

let main () =
    if Array.length (Sys.argv) = 2 then
        if Sys.argv.(1) = "-help" || Sys.argv.(1) = "--help" then
            print_endline "Refer to README"
        else
            maincommande ()
    else
        Inter.main ()

let _ = main ()
