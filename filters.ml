(* Permet de faire le "gris du pixel" *)
let level (r,g,b) = 
(0.3 *. (float r) +. 0.59 *. (float g) +. 0.11 *. (float b)) /. 255.

(* Applique level à un RGB *)
let color2grey (r,g,b) =
  let grey = int_of_float (level (r,g,b) *. 255.) in
    (grey, grey, grey)

(* Faire le "gris" sur toute l'image *)
let image2grey src dst = 
  let (width,height) = Imagehelp.get_dims src in
     for i = 0 to height - 1 do
       for j = 0 to width - 1 do
	 Sdlvideo.put_pixel_color dst j i (color2grey 
		(Sdlvideo.get_pixel_color src j i))
       done
     done

(* Binarise l'image (le seuil est calculer avec otsu.ml) *)
let binarize img =
  let threshold = Otsu.threshold_img img in
    let (w,h) = Imagehelp.get_dims img  in
      for i = 0 to w -1 do
	for j = 0 to h - 1 do
	  let (r,_,_) = Sdlvideo.get_pixel_color img i j in
	  let color = if r > threshold then 255 else 0 in
	  Sdlvideo.put_pixel_color img i j (color,color,color);
	done
      done

(* On compare deux level : utile pour le Array.sort *)
let comparelevel rgb1 rgb2 =
  let a = int_of_float (level rgb1 *. 255.) 
  and b = int_of_float (level rgb2 *. 255.) in
  if a = b then
    0
  else
    if a < b then
      a - b
    else
      a + b

(* Applique le filtre median *)
let median_filter src dst =
  let (w,h) = Imagehelp.get_dims src in
    for x = 0 to w - 1 do
      for y = 0 to h - 1 do
	let list_res = Imagehelp.getallneighbors src x y in
        let resultlist = List.sort (comparelevel) list_res in
        let medianindex = (List.length resultlist) / 2 in
        let medianpixel = List.nth resultlist medianindex in
        Sdlvideo.put_pixel_color dst x y medianpixel
      done;
    done

let gaussian =
        [|[|1. /. 16.;2. /. 16.;1. /. 16.|] ;
          [|2. /. 16.;4. /. 16.;2. /. 16.|] ;
          [|1. /. 16.;2. /. 16.;1. /. 16.|]|]

(* Le filtre de convolution *)
let convolution_filter src dst kernel =
  let (w,h) = Imagehelp.get_dims src in
  let size = Array.length kernel in
  for x = 0 to w - 1 do
    for y = 0 to h - 1 do
      let rescolor = ref 0.0 in
	for fX = 0 to size - 1 do
	for fY = 0 to size - 1 do
	  let imageX = (x - size / 2 + fX + w) mod w
	  and imageY = (y - size / 2 + fY + h) mod h in
	  let (r,_,_) = Sdlvideo.get_pixel_color src imageX imageY in
	  rescolor := !rescolor +. (float_of_int (r)) 
			*. kernel.(fX).(fY)
	done;
	done;
      let color = if !rescolor > 255. then 255 
		  else int_of_float !rescolor in
      Sdlvideo.put_pixel_color dst x y (color,color,color);
    done
  done
