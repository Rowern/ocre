(* Classe pour appeler les fonctions en mode easy *)
let load path = Imagehelp.load path

let save img path = Imagehelp.save img path

let grey path =
begin
    let surf = load path in
    Filters.image2grey surf surf;
    print_endline "Greyscale :      OK";
    save surf path;
    print_endline "Saving :         OK"
end

let binarize path =
begin
    let surf = load path in
    Filters.binarize surf;
    print_endline "Binarisation :   OK";
    save surf path;
    print_endline "Saving :         OK"
end

let median path =
begin
    let surf1 = load path in
    let surf = surf1 in
    Filters.median_filter surf surf1;
    print_endline "Median :         OK";
    save surf1 path;
    print_endline "Saving :         OK"
end

let convol path =
begin
    let surf = load path in
    Filters.convolution_filter surf surf (Filters.gaussian);
    print_endline "Gaussian :       OK";
    save surf path;
    print_endline "Saving :         OK"
end

let rotation path =
begin
    let surf = load path in
    let surf2 = load path in
    Rotate.redress surf surf2;
    print_endline "Rotation :       OK";
    save surf2 path;
    print_endline "Saving :         OK"
end

let segment path =
begin
    let surf = load path in
    Segment.do_the_seg surf;
    print_endline "Segmentation :   OK";
    save surf path;
    print_endline "Saving :         OK"
end

let run_all path =
begin
    let surf = load path in
    let surf2 = load path in
    Filters.image2grey surf surf;
    print_endline "Greyscale :      OK";
    Filters.convolution_filter surf surf (Filters.gaussian);
    print_endline "Gaussian :       OK";
    Filters.binarize surf;
    print_endline "Binarisation :   OK";
    Rotate.redress surf surf2;
    print_endline "Rotation :       OK";
    Segment.do_the_seg surf2;
    print_endline "Segmentation :   OK";
    save surf2 path;
    print_endline "Saving :         OK";
end

let net =
    Network.main ()
